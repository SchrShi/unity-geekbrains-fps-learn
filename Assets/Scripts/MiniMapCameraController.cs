﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class MiniMapCameraController : MonoBehaviour
{
    private Transform _player;

    private float _distance = 400.0f;

    private void Start()
    {
        _player = FindObjectOfType<FirstPersonController>().transform;
    }

    private void LateUpdate()
    {
        transform.position = _player.position + Vector3.up * _distance;
    }
}
