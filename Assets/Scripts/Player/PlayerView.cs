﻿using System;
using TMPro;
using UnityEngine;


namespace CyberWarrior
{
    public class PlayerView : MonoBehaviour
    {
        #region Fields

        public event Action<float> Adjust;

        private Canvas _canvas;

        private TextMeshProUGUI _healthTMP;

        private float _maxHealth = 100.0f;
        private float _currentHealth;
        private float _coolDownPerAttack = 3.0f;
        private float _currentCoolDown = 0;

        #endregion

        #region UnityMethods

        private void Start()
        {
            _currentHealth = _maxHealth;
            foreach(var canvas in FindObjectsOfType<Canvas>()) 
                {
                if (canvas.CompareTag("Player")) _canvas = canvas;
                }
            foreach (var obj in _canvas.gameObject.GetComponentsInChildren<TextMeshProUGUI>())
            {
                if (obj.name == "HealthText") _healthTMP = obj;
            }
            if (_healthTMP != null)
                _healthTMP.SetText($"{_currentHealth} HP");
            else print("Не могу найти TMP");
            _canvas.worldCamera = Camera.main;
            _canvas.planeDistance = 0.1f;
        }

        private void Update()
        {
            if (_currentCoolDown <= 0)
            {
                if(_currentHealth < 100)
                ChangeHealth(-(Time.deltaTime * 2.5f));
            }
            else _currentCoolDown -= Time.deltaTime;
        }

        #endregion

        #region Methods

        public void ChangeHealth(float health)
        {
                var lastHealth = _currentHealth;
                _currentHealth -= health / 2.5f;
                if (lastHealth > _currentHealth) _currentCoolDown = _coolDownPerAttack;
                _healthTMP.SetText($"{Mathf.Round(_currentHealth)} HP");
                Adjust?.Invoke(health);
        }

        public void Death()
        {
            // To-Do
            Destroy(gameObject);
        }

        #endregion
    }
}