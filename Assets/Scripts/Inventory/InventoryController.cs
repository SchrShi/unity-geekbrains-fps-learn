﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

namespace CyberWarrior
{
    public static class InventoryController
    {

        #region Fields

        private static Dictionary<WeaponType, GunData> _weaponDictionary = new Dictionary<WeaponType, GunData>();
        private static GunController _controller;
        private static Canvas _canvas;
        private static GameObject _currentWeapon;
        private static IPerson _player;

        #endregion



        #region Properties

        public static int Count 
        { 
            get
            {
                return _weaponDictionary.Count;
            }
            private set { }
        }

        #endregion


        #region Methods

        public static void InitializeInventory(IPerson player)
        {
            if (_weaponDictionary.Count <= 0)
            {
                _weaponDictionary.Add(WeaponType.Pistol, null);
                _weaponDictionary.Add(WeaponType.Lazer, null);
                _weaponDictionary.Add(WeaponType.Sniper, null);
            }
            foreach (var obj in GameObject.FindObjectsOfType<Canvas>()) if (obj.gameObject.CompareTag("Player")) _canvas = obj;
            GameObject.DontDestroyOnLoad(_canvas.gameObject);
            _player = player;
        }

        public static void NewWeapon(GunData weapon, Transform playerTransform)
        {
            var newWeaponStruct = weapon.GunStruct;
            var index = _weaponDictionary.Where(slot => slot.Key == newWeaponStruct.WeaponType).FirstOrDefault().Key;
            if (_weaponDictionary[index] != null)
            {
                var oldWeapon = GameObject.Instantiate(_weaponDictionary[index].GunStruct.GunPrefab, playerTransform.position, Quaternion.identity);
                oldWeapon.transform.parent = null;
                oldWeapon.SetActive(true);
                oldWeapon.GetComponent<Rigidbody>().isKinematic = false;
                oldWeapon.layer = LayerMask.NameToLayer("Weapon");
            }
            _weaponDictionary[index] = weapon;
            

            Image image = null;
            foreach (var obj in _canvas.gameObject.GetComponentsInChildren<Image>()) if (obj.gameObject.name == index.ToString()) image = obj;
            if (newWeaponStruct.GunIcon == null) Debug.Log("Нет иконки у оружия");
            else if (image.gameObject == null) Debug.Log("Не могу найти картинку для иконки");
            image.sprite = newWeaponStruct.GunIcon;

        }

        public static void PickWeapon(int index, ref Transform rightHandPoint, ref Transform leftHandPoint)
        {
            var indexWeapon = (WeaponType)index;
            if (_weaponDictionary[indexWeapon] == null || index > _weaponDictionary.Count || _weaponDictionary[indexWeapon].GunStruct.GunPrefab == _currentWeapon) return;
            else
            {
                if (_controller != null)
                {
                    _controller.OnDisable();
                }
                
                    GameObject.Destroy(_currentWeapon);
                    
                    _currentWeapon = GameObject.Instantiate(_weaponDictionary[indexWeapon].GunStruct.GunPrefab);
                    _currentWeapon.transform.parent = Camera.main.transform;
                    _currentWeapon.transform.localPosition = _weaponDictionary[indexWeapon].GunStruct.PositionFromCamera;
                    _currentWeapon.transform.localRotation = Quaternion.Euler(_weaponDictionary[indexWeapon].GunStruct.RotationFromCamera);
                    _currentWeapon.layer = LayerMask.NameToLayer("Ignore Raycast");
                    _currentWeapon.GetComponent<Rigidbody>().isKinematic = true;
                    foreach (var point in _currentWeapon.GetComponentsInChildren<Transform>())
                    {
                        if (point.gameObject.CompareTag("Hand"))
                        {
                            if (point.gameObject.name == "RightHandPoint") rightHandPoint = point;
                            else if (point.gameObject.name == "LeftHandPoint") leftHandPoint = point;
                        }
                    }
                    _controller = new GunController(_currentWeapon, _weaponDictionary[indexWeapon], _player, Camera.main.transform, LayerMask.NameToLayer("Player"));
                
            }
        }

        #endregion
    }
}
