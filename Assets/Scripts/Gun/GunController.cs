﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace CyberWarrior
{
    public class GunController
    {
        #region Fields

        public event Action<bool> BulletIsNull;

        private Transform _startBulletPoint;
        private GunData _gunData;
        private GameObject _particleShoot;
        private GameObject _shootHit;
        private GameObject _gun;
        private Rigidbody _rigidbody;
        private LayerMask _ignoreMask;
        private float fireSoundVolume = 100.0f;
        private float _nextFire = 0.0f;
        private float _bulletInMagazine;
        private IPerson _person;
        private TextMeshProUGUI _bulletViewer;

        #endregion


        #region Methods

        public GunController(GameObject gun, GunData gunData, IPerson person, Transform startBulletPoint, LayerMask mask)
        {
            _gunData = gunData;
            _startBulletPoint = startBulletPoint;
            person.Shoot += Shoot;
            foreach(var child in gun.GetComponentsInChildren<ShootParticleCheker>())
            {
                _particleShoot = child.gameObject;
            }
            _gun = gun;
            _ignoreMask = mask;
            _bulletInMagazine = _gunData.GunStruct.bulletCount;
            _person = person;
            _rigidbody = _gun.GetComponent<Rigidbody>();
            _rigidbody.isKinematic = true;
            onInizialize();
            OnEnable();
        }

        public GunController(GameObject gun, IPerson person, Transform startBulletPoint, LayerMask mask)
        {
            _gunData = gun.GetComponent<DataHelper>()._data;
            _startBulletPoint = startBulletPoint;
            foreach (var child in gun.GetComponentsInChildren<ShootParticleCheker>())
            {
                child.Person = person;
                _particleShoot = child.gameObject;
            }
            _gun = gun;
            _ignoreMask = mask;
            _bulletInMagazine = _gunData.GunStruct.bulletCount;
            _person = person;
            _gun.GetComponent<Rigidbody>().isKinematic = true;
            onInizialize();
            OnEnable();
        }

        private void onInizialize()
        {
            _bulletViewer = _gun.GetComponentInChildren<TextMeshProUGUI>();
            _bulletViewer.text = _bulletInMagazine.ToString();
                
        }

        public void OnEnable()
        {
            if (_particleShoot.GetComponent<ShootParticleCheker>() != null)
                _person.Shoot += _particleShoot.GetComponent<ShootParticleCheker>().ShootParticle;
            _person.Shoot += Shoot;
            _person.Death += Drop;
        }

        private void Drop()
        {
            _gun.AddComponent<DataHelper>()._data = _gunData;
            _gun.transform.parent = null;
            _gun.GetComponent<Rigidbody>().isKinematic = false;
            OnDisable();
        }

        private void Shoot(bool isShoot)
        {
            
                if (isShoot && Time.time > _nextFire)
                {
                if(_bulletInMagazine > 0) 
                {
                    RaycastHit hit;
                        _nextFire = Time.time + 1.0f / _gunData.GunStruct.FireRate;
                    _bulletInMagazine--;
                    _bulletViewer.text = _bulletInMagazine.ToString();
                    if (_gun != null)
                    AudioSource.PlayClipAtPoint(_gunData.GunStruct.FireSound, _gun.transform.position, fireSoundVolume);
                    
                    int mask = 1 << _ignoreMask;
                    mask = ~mask;
                    if(Physics.Raycast(_startBulletPoint.position, _startBulletPoint.forward * _gunData.GunStruct.HitDistance, out hit, _gunData.GunStruct.HitDistance, mask))
                    {
                        
                            hit.collider.gameObject.GetComponent<PlayerView>()?.ChangeHealth(_gunData.GunStruct.Damage);
                            hit.collider.gameObject.GetComponent<EnemyView>()?.Hit(_gunData.GunStruct.Damage);
                            if (hit.collider.gameObject != null && _gun != null && _gunData != null)
                                hit.rigidbody?.AddForce((hit.transform.position - _gun.transform.position) * _gunData.GunStruct.ShootForce, ForceMode.Impulse);
                            _shootHit = Object.Instantiate(_gunData.GunStruct.ObjectWithFireParticles, hit.point, _startBulletPoint.rotation);
                            _shootHit.AddComponent<HitParticleCheker>();
                       
                    }
                    BulletIsNull?.Invoke(false);
                }
                else
                {
                    _bulletViewer.text = "Low Ammo";
                    BulletIsNull?.Invoke(true);
                    _bulletInMagazine = _gunData.GunStruct.bulletCount;
                }
                
        }
        }

        public void OnDisable()
        {
            if (_person != null)
            {
                if(_particleShoot!=null)
                _person.Shoot -= _particleShoot.GetComponent<ShootParticleCheker>().ShootParticle;
                _person.Shoot -= Shoot;
                _person.Death -= Drop;
            }
        }

        #endregion
    }
}
