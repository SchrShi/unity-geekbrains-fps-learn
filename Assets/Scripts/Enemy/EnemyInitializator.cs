﻿using UnityEngine;

namespace CyberWarrior
{
    public class EnemyInitializator
    {
        private GameObject EnemyObject;

        public EnemyInitializator(EnemyData data, Vector3 spawnPosition, GameManager manager)
        {
            EnemyObject = Object.Instantiate<GameObject>(data.EnemyStruct.EnemyObject, spawnPosition, Quaternion.identity);
            Transform hand = null;
            foreach (Transform objTransform in EnemyObject.GetComponentsInChildren<Transform>())
            {
                if (objTransform.gameObject.CompareTag("Hand")) { hand = objTransform; }
            }
            var gunData = data.EnemyStruct.GunData;
            var gun = Object.Instantiate(gunData.GunStruct.GunPrefab);
            

            var enemyController = new EnemyController(manager.PlayerObject.transform, EnemyObject, data, gun, gunData);
            if (hand != null && data.EnemyStruct.GunData.GunStruct.GunPrefab != null)
            {
                gun.transform.parent = hand;
                gun.transform.localPosition = new Vector3(-0.01411421f, 0.04722619f, 0.1207426f);
                gun.transform.localRotation = Quaternion.Euler(new Vector3(-65.289f, 97.51401f, -10.05f));
                Transform startBulletPoint = null;
                foreach (var child in gun.GetComponentsInChildren<Transform>())
                {
                    if (child.gameObject.CompareTag("StartBulletPoint")) startBulletPoint = child;
                }
                new GunController(gun, gunData, enemyController, startBulletPoint, EnemyObject.layer);
            }


            manager.AddToFixedUpdatable(enemyController);
            manager.AddToUpdatable(enemyController);
        }
    }
}
