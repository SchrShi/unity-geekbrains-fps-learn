﻿using System;
using UnityEngine;


namespace CyberWarrior
{
    [Serializable]
    public struct EnemyStruct
    {
        public GameObject EnemyObject;
        public GunData GunData;
        public Sprite SpriteFromBullet;

        public float Health;
        public float Speed;
    }
}
