﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;


namespace CyberWarrior 
{
    public class EnemyController : IUpdatable, IFixedUpdatable, IPerson
    {
        #region Fields

        public event Action<bool> Target;
        public event Action<bool> Movement;
        public event Action<bool> Shoot;
        public event Action Death;

        private Collider[] _bonesForRagdolls;
        private Rigidbody[] _rigidbodiesForRagdolls;
        private Transform _playerTransform;
        private GameObject _enemyObject;
        private GameObject _gunObject;
        private Rigidbody _rigidbody;
        private EnemyData _enemyData;
        private GunData _gunData;
        private EnemyView _enemyView;
        private bool _isTarget = false;
        private bool _isMissing = true;
        private bool _isDeath = false;
        private float _pauseForMissing = 15.0f;
        private float _health;

        #endregion


        #region Contructor

        public EnemyController(Transform player, GameObject enemyObject, EnemyData enemyData, GameObject gun, GunData gunData)
        {
            _playerTransform = player;
            _enemyObject = enemyObject;
            _gunObject = gun;
            _enemyData = enemyData;
            _gunData = gunData;
            _rigidbody = enemyObject.GetComponent<Rigidbody>();
            _rigidbody.isKinematic = true;
            enemyObject.AddComponent<EnemyView>().EnemyController = this;
            _enemyView = enemyObject.GetComponent<EnemyView>();
            _health = enemyData.EnemyStruct.Health;

            OnEnabled();
            BonesContruct();
            Schrodinger(true);
        }

        #endregion


        #region Methods

        private void BonesContruct()
        {
            _bonesForRagdolls = _enemyObject.GetComponentsInChildren<Collider>();
            _rigidbodiesForRagdolls = _enemyObject.GetComponentsInChildren<Rigidbody>();
        }

        private void OnEnabled()
        {
            Target += _enemyView.Target;
            Movement += _enemyView.Movement;
            Death += _enemyView.Death;
            _enemyView.Damage += Damage;
        }

        public void Tick()
        {
            if(_enemyObject != null && !_isDeath)
            {
                ForMissingTimer();
            }
        }

        public void FixedTick()
        {
            if (_enemyObject != null && !_isDeath)
            {
                WatchingPlayer();
                Target.Invoke(_isTarget);
            }
        }

        private void ForMissingTimer()
        {
            if (_isMissing)
            {
                if (_pauseForMissing > 0)
                    _pauseForMissing -= Time.deltaTime;
                else
                {
                    _isTarget = false;
                    _pauseForMissing = 15.0f;
                }
            }
        }

        private void WatchingPlayer()
        {
                RaycastHit hit;
                Vector3 playerPosition = _playerTransform.position - _enemyObject.transform.position;
                var enemyPosition = new Vector3(_enemyObject.transform.position.x, _enemyObject.transform.position.y + 1.9f, _enemyObject.transform.position.z);
                Ray ray = new Ray(enemyPosition, new Vector3(playerPosition.x, playerPosition.y - 1.0f, playerPosition.z));
                var raycast = Physics.Raycast(ray, out hit, 40.0f);
                Debug.DrawRay(enemyPosition, new Vector3(playerPosition.x, playerPosition.y - 1.0f, playerPosition.z), Color.red);
                if (raycast)
                {
                    if (hit.collider.gameObject.CompareTag("Player"))
                    {
                        Movement.Invoke(false);
                        _isTarget = true;
                        _isMissing = false;
                        _enemyObject.transform.rotation = Quaternion.LookRotation(playerPosition);
                        Shoot.Invoke(true);
                    }
                }

                else if (!raycast || !hit.collider.gameObject.CompareTag("Player"))
                {
                    if (_isTarget)
                    {
                        _isMissing = true;
                        Movement.Invoke(true);
                        Vector3 movement = (_playerTransform.position - _enemyObject.transform.position).normalized * _enemyData.EnemyStruct.Speed;
                        movement.y = _rigidbody.velocity.y;
                        _rigidbody.velocity = movement;
                        _enemyObject.transform.rotation = Quaternion.LookRotation(playerPosition);
                        Shoot.Invoke(false);
                    }
                }
        }
        
        public void Damage(float damage)
        {
            _health -= damage;
            if (_health <= 0) onDeath();
        }

        private void onDeath()
        {
            _isDeath = true;
            Death?.Invoke();
            Schrodinger(false);
            OnDisable();
            _enemyView.enabled = false;
        }

        private void OnDisable()
        {
            _enemyView.Damage -= Damage;
            
        }

        private void Schrodinger(bool isAlive)
        {
            for (int i = 1; i < _bonesForRagdolls.Length; i++)
            {
                _rigidbodiesForRagdolls[i].isKinematic = isAlive;
                _bonesForRagdolls[i].enabled = !isAlive;
            }
            _enemyObject.GetComponent<Animator>().enabled = isAlive;
            _bonesForRagdolls[0].enabled = isAlive;
            _rigidbodiesForRagdolls[0].isKinematic = !isAlive;
        }

        #endregion
    }
}
