﻿using UnityEngine;

public interface IUpdatable
{
    void Tick();

}
