﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.CrossPlatformInput;


namespace CyberWarrior
{
    public class MyMouseLook : MonoBehaviour
    {
        #region Fields
        [SerializeField] private GameObject _fpsObject;
        [SerializeField] private Transform _hand;
        [SerializeField] private float _minimumY = -25.0f;
        [SerializeField] private float _maximumY = 25.0f;
        private MouseLook _mouseLook;
        private Quaternion _initRotation;

        Quaternion _antiSeaCamera;
        private float _yRot;

        #endregion


        #region UnityMethods

        private void Start()
        {
            _initRotation = transform.localRotation;

            _mouseLook = _fpsObject.GetComponent<FirstPersonController>().m_MouseLook;
            _antiSeaCamera = Camera.main.transform.rotation;
        }

        private void LateUpdate()
        {
            _yRot += CrossPlatformInputManager.GetAxis("Mouse Y") * _mouseLook.YSensitivity;

            if (_yRot < _minimumY) _yRot = _minimumY;
            if (_yRot > _maximumY) _yRot = _maximumY;

            transform.localRotation = new Quaternion(_initRotation.x * _yRot, transform.localRotation.x, transform.localRotation.z, transform.localRotation.w);
            Camera.main.transform.localRotation = Quaternion.Euler(Camera.main.transform.rotation.x, Camera.main.transform.rotation.y, _initRotation.z);

        }

        #endregion
    }
}
