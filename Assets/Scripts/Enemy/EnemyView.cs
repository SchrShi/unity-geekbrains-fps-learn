﻿using System;
using UnityEngine;


namespace CyberWarrior
{
    public class EnemyView : MonoBehaviour
    {
        public event Action<float> Damage;
        public EnemyController EnemyController;

        private Animator _animator;


        private void Start()
        {
            _animator = gameObject.GetComponent<Animator>();
        }

        private void OnDisable()
        {
            EnemyController.Target -= Target;
            EnemyController.Movement -= Movement;
            EnemyController.Death -= Death;
        }

        public void Target(bool isTarget)
        {
            _animator.SetBool("WatchEnemy", isTarget);
        }

        public void Movement(bool isMovement)
        {
            //Пока костыль, потом сделаю нормально
            if (isMovement)
                _animator.SetInteger("VerticalMovement", 2);
            else _animator.SetInteger("VerticalMovement", 0);
        }

        public void Hit(float damage)
        {
            Damage?.Invoke(damage);
        }

        public void Death()
        {
            MeshRenderer icon = null;
            foreach(var mesh in gameObject.GetComponentsInChildren<MeshRenderer>())
            {
                if (mesh.gameObject.layer == LayerMask.NameToLayer("Icons")) icon = mesh;
            }
            if(icon != null)
            {
                icon.material.SetColor("_Color", Color.black);
            }
        }
    }
}