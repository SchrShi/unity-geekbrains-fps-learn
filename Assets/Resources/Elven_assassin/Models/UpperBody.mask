%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: UpperBody
  m_Mask: 01000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: face
    m_Weight: 1
  - m_Path: metarig
    m_Weight: 1
  - m_Path: metarig/hips
    m_Weight: 1
  - m_Path: metarig/hips/hips_001_L_001
    m_Weight: 1
  - m_Path: metarig/hips/hips_001_L_001/hips_001_L_002
    m_Weight: 1
  - m_Path: metarig/hips/hips_001_R_001
    m_Weight: 1
  - m_Path: metarig/hips/hips_001_R_001/hips_001_R_002
    m_Weight: 1
  - m_Path: metarig/hips/spine
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head/head_002
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head/head_002/head_002_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head/head_002/head_002_L/head_002_L_001
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head/head_002/head_002_L/head_002_L_001/head_002_L_002
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head/head_002/head_002_L/head_002_L_001/head_002_L_002/head_002_L_003
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head/head_002/head_002_L/head_002_L_001/head_002_L_002/head_002_L_003/head_002_L_004
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head/head_002/head_002_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head/head_002/head_002_R/head_002_R_001
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head/head_002/head_002_R/head_002_R_001/head_002_R_002
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head/head_002/head_002_R/head_002_R_001/head_002_R_002/head_002_R_003
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head/head_002/head_002_R/head_002_R_001/head_002_R_002/head_002_R_003/head_002_R_004
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R
    m_Weight: 1
  - m_Path: metarig/hips/thigh_L
    m_Weight: 1
  - m_Path: metarig/hips/thigh_L/shin_L
    m_Weight: 1
  - m_Path: metarig/hips/thigh_L/shin_L/foot_L
    m_Weight: 1
  - m_Path: metarig/hips/thigh_L/shin_L/foot_L/toe_L
    m_Weight: 1
  - m_Path: metarig/hips/thigh_R
    m_Weight: 1
  - m_Path: metarig/hips/thigh_R/shin_R
    m_Weight: 1
  - m_Path: metarig/hips/thigh_R/shin_R/foot_R
    m_Weight: 1
  - m_Path: metarig/hips/thigh_R/shin_R/foot_R/toe_R
    m_Weight: 1
