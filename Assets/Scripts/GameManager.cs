﻿using UnityEngine;
using System.Collections.Generic;

namespace CyberWarrior
{
    public class GameManager : MonoBehaviour
    {
        #region Fields

        [Header("Player")]
        [SerializeField] private GameObject _playerPrefab;
        [SerializeField] private Vector3 _playerStartPoint;
        [HideInInspector] public GameObject PlayerObject { get; private set; }

        [Header("Enemy")]
        [SerializeField] private float _distanceForSpawnEnemy;
        [SerializeField] private EnemyData[] _enemyTypes;
        [SerializeField] private List<Transform> _spawnPointList;
        [SerializeField] private int _maxEnemyOnPoint;

        private PlayerController _playerController;
        private PersonModel _playerModel;
        
        private List<IUpdatable> _updatables = new List<IUpdatable>();
        private List<IFixedUpdatable> _fixedUpdatables = new List<IFixedUpdatable>();

        #endregion


        #region UnityMethods

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        private void Update()
        {
            foreach (var updatable in _updatables)
                    updatable.Tick();
            DistanceForSpawningEnemyChecked();
        }

        private void FixedUpdate()
        {
                foreach (var obj in _fixedUpdatables)
                {
                    obj.FixedTick();
                }
        }

        private void OnLevelWasLoaded(int level)
        {
            _spawnPointList = new List<Transform>();
            foreach (var point in GameObject.FindGameObjectsWithTag("Respawn"))
            {
                if (point.layer == LayerMask.NameToLayer("Enemy")) _spawnPointList.Add(point.transform);
                if (point.layer == LayerMask.NameToLayer("Player")) _playerStartPoint = point.transform.position;
            }
            _playerModel = new PersonModel();
            PlayerObject = Instantiate(_playerPrefab, _playerStartPoint, Quaternion.identity);
            var playerView = PlayerObject.GetComponent<PlayerView>();

            _playerController = new PlayerController(_playerModel, playerView);
            AudioListener.volume = SettingModel.Voiume;
        }
        #endregion


        #region Methods

        public void AddToUpdatable(IUpdatable updatable)
        {
            _updatables.Add(updatable);
        } 

        public void AddToFixedUpdatable(IFixedUpdatable fixedUpdatable)
        {
            _fixedUpdatables.Add(fixedUpdatable);
        }

        private void DistanceForSpawningEnemyChecked()
        {
            if(_spawnPointList.Count > 0) {
                for (int i = 0; i < _spawnPointList.Count; i++)
                {
                    if ((PlayerObject.transform.position - _spawnPointList[i].position).sqrMagnitude <= _distanceForSpawnEnemy * _distanceForSpawnEnemy)
                    {
                        var enemyCount = Random.Range(1, _maxEnemyOnPoint);
                        for (int j = 0; j < enemyCount; j++)
                        {
                            var enemyEl = Random.Range(0, _enemyTypes.Length);
                            new EnemyInitializator(_enemyTypes[enemyEl], _spawnPointList[i].position, this);
                        }
                        var point = _spawnPointList[i];
                        _spawnPointList.Remove(point);
                        Destroy(point.gameObject);
                    }
                }
            }
        }

        #endregion

    }
}
