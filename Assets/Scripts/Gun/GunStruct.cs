﻿using System;
using UnityEngine;


namespace CyberWarrior
{
    [Serializable]
    public struct GunStruct
    {
        public Sprite GunIcon;
        public GameObject GunPrefab;
        public WeaponType WeaponType;
        public GameObject ObjectWithFireParticles;
        public AudioClip FireSound;
        public Vector3 PositionFromCamera;
        public Vector3 RotationFromCamera;
        public float FireRate;
        public float Damage;
        public float bulletCount;
        public float HitDistance;
        public float ShootForce;
    }
}
