﻿using System;

public interface IPerson
{
    event Action<bool> Shoot;
    event Action Death;
}
