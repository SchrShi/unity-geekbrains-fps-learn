﻿using UnityEngine;


namespace CyberWarrior
{
    public class AnimatorForNpc : MonoBehaviour
    {
        #region Fields

        private Transform _obj;
        private Animator _animator;

        #endregion


        #region UnityMethods

        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        private void Update()
        {

            //Все неинтерактивные объекты у меня под маской Default.
            var defaultLayer = 1 << LayerMask.NameToLayer("Default");
            defaultLayer = ~defaultLayer;
            Collider[] colliders = Physics.OverlapSphere(transform.position, 2.0f, defaultLayer);

            if (colliders.Length > 0)
            {
                for (int i = 1; i < colliders.Length; i++)
                {
                    if ((colliders[i - 1].transform.position - transform.position).sqrMagnitude > (colliders[i].transform.position - transform.position).sqrMagnitude)
                    {
                        (colliders[i - 1], colliders[i]) = (colliders[i], colliders[i - 1]);
                    }
                }
                _obj = colliders[0].transform;
            }
        }

        private void OnAnimatorIK(int layerIndex)
        {
            if (_animator)
            {
                if (_obj != null && (_obj.position - transform.position).sqrMagnitude <= 4)
                {
                    _animator.SetLookAtWeight(1);
                    _animator.SetLookAtPosition(_obj.position);
                }
            }
        }

        #endregion
    }
}
