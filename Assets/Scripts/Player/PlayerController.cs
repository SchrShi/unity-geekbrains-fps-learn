﻿namespace CyberWarrior
{
    public class PlayerController
    {
        #region Fields

        private PlayerView _playerView;
        private PersonModel _playerModel;

        #endregion


        #region Constructor

        public PlayerController(PersonModel playerModel, PlayerView playerView)
        {
            _playerModel = playerModel;
            _playerView = playerView;

        }

        #endregion


        #region Methods

        public void OnEnable()
        {
            _playerModel.Death += Death;
            _playerView.Adjust += _playerModel.SetNewHealth;
        }


        public void Death()
        {
            _playerView.Death();
            OnDisable();
        }

        public void OnDisable()
        {
            _playerModel.Death -= Death;

        }

        #endregion
    }
}
