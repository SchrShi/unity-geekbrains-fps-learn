﻿using UnityEngine;


namespace CyberWarrior {
    public class ShootParticleCheker : MonoBehaviour
    {

        public IPerson Person;

        private ParticleSystem _particle;

        private void Start()
        {
            _particle = GetComponent<ParticleSystem>();
        }

        private void OnDisable()
        {
            if(Person != null)
            Person.Shoot -= ShootParticle;
        }

        private void OnDestroy()
        {
            OnDisable();
            Destroy(_particle);
        }

        public void ShootParticle(bool isShoot)
        {
            if (isShoot)
            {
                _particle.Play();
            }
            transform.GetChild(0).gameObject.SetActive(isShoot);
        }
    }
}
