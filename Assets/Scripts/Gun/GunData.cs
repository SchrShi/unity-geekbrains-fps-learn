﻿using UnityEngine;


namespace CyberWarrior
{
    [CreateAssetMenu(fileName = "New Gun Data", menuName = "Data/Gun")]
    public class GunData : ScriptableObject, IData
    {
        public GunStruct GunStruct;
    }
}
