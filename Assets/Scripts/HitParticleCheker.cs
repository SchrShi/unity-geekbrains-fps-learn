﻿using UnityEngine;


namespace CyberWarrior {
    public class HitParticleCheker : MonoBehaviour
    {

        private ParticleSystem _particle;

        private void Start()
        {
            _particle = GetComponent<ParticleSystem>();
            _particle.Play();
        }


        private void Update()
        {
            if (!_particle.isPlaying)
            {
                Destroy(gameObject);
            }
    }
    }
}
