﻿using System;
using UnityEngine;

public class PersonModel
{
    #region Actions

    public event Action Death;
    public event Action<float> ChangeHealth;
    public event Action<string> ChangeWeapon;
    public event Action<GameObject> Pickup;

    #endregion


    #region Fields

    private float _health = 100.0f;
    private float _currentHealth;

    #endregion


    #region Contructor

    public PersonModel()
    {
        _currentHealth = _health;
    }

    #endregion


    #region Methods

    public void SetNewHealth(float health)
    {
        _currentHealth -= health;
        if (_currentHealth < 0) Death?.Invoke();
        else ChangeHealth?.Invoke(_currentHealth);
        ChangeHealth.Invoke(_currentHealth);
    }

    #endregion
}
