﻿using System;
using UnityEngine;

public class CollisionHelper : MonoBehaviour
{
    public event Action<Transform> CollisionWithBullet;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            CollisionWithBullet.Invoke(collision.transform);
        }
    }
}
