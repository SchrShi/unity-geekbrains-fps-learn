﻿using UnityEngine;

public class CubeChangeColor : MonoBehaviour
{
    [SerializeField] private float AreaWidth;
    [SerializeField] private float AreaHeight;
    private float _redValue;
    private float _greenValue;
    private float _blueValue;
    private float _transparentValue = 1.0f;
    private bool _isStay;
    private Renderer _mesh;

    private void OnGUI()
    {
        if (_isStay)
        {
            GUILayout.BeginArea(new Rect((Screen.width / 2) - (AreaWidth / 2) , (Screen.height / 2) - (AreaHeight / 2), AreaWidth, AreaHeight));
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Поменяй цвет одним прикосновением!");
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            if (GUILayout.RepeatButton("Min"))
                _redValue = 0.0f;
            _redValue = GUILayout.HorizontalSlider(_redValue, 0.0f, 1.0f);
            if (GUILayout.RepeatButton("Max"))
                _redValue = 10.0f;
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            if (GUILayout.RepeatButton("Min"))
                _greenValue = 0.0f;
            _greenValue = GUILayout.HorizontalSlider(_greenValue, 0.0f, 1.0f);
            if (GUILayout.RepeatButton("Max"))
                _greenValue = 10.0f;
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            if (GUILayout.RepeatButton("Min"))
                _blueValue = 0.0f;
            _blueValue = GUILayout.HorizontalSlider(_blueValue, 0.0f, 1.0f);
            if (GUILayout.RepeatButton("Max"))
                _blueValue = 10.0f;
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            if (GUILayout.RepeatButton("Min"))
                _transparentValue = 0.0f;
            _transparentValue = GUILayout.HorizontalSlider(_transparentValue, 0.0f, 1.0f);
            if (GUILayout.RepeatButton("Max"))
                _transparentValue = 10.0f;
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndArea();
        }
    }

    void Start()
    {
        _mesh = GetComponent<Renderer>();
    }

    void Update()
    {
        var color = new Color(_redValue, _greenValue, _blueValue, _transparentValue);
        _mesh.sharedMaterial.color = color;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _isStay = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) _isStay = false;
    }
}
