﻿using UnityEngine;


namespace CyberWarrior
{
    [CreateAssetMenu(fileName = "New Enemy Data", menuName = "Data/EnemyData")]
    public sealed class EnemyData : ScriptableObject
    {
        public EnemyStruct EnemyStruct;

    }
}